<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DonationCategoryController;
use App\Http\Controllers\CharityProjectController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\DonationController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    // without required token routes
    Route::post('register', [UserController::class, 'register']);
    Route::post('login', [UserController::class, 'authenticate']);
    //forgot password 
    Route::post('sendPasswordResetLink', 'App\Http\Controllers\PasswordResetRequestController@sendPasswordResetEmail'); // Send Link
    Route::post('resetPassword', 'App\Http\Controllers\ChangePasswordController@passwordResetProcess');                 // Reset Password


    // token requried routes
    Route::group(['middleware' => ['jwt.verify']], function() {
        
        //get single user information
        Route::get('user', [UserController::class, 'getAuthenticatedUser']); // get login user information
        Route::post('profile/{id}', [UserController::class, 'update']);      // update user profile
        Route::post('addpaymentmethod', [UserController::class, 'create_payment_methods']);      // update user profile  
        Route::get('paymentmethod/{id}', [UserController::class, 'show_payment_methods']);       // get current user payment methods   

        //Donation Categories
        Route::get('charity', [DonationCategoryController::class, 'index']);  // get all donation categories

        //Charity Resource Projects 
        Route::resource('projects', CharityProjectController::class); // get all charity Projects
        Route::get('category/{id}', [CharityProjectController::class, 'showByCategory']);  // get charity category wise donation
        
        // Videos Resource
        Route::resource('videos', VideoController::class); // get all charity Projects

        //Make Donation
        Route::post('donation', [DonationController::class, 'create_donation']);      // create Donation
        Route::get('donation/{id}', [DonationController::class, 'show_user_donation']);  // get current user donation
        Route::get('mycharity/{id}', [DonationController::class, 'show_user_projects']);  // get current user projects
        Route::get('charitydonation/{id}', [DonationController::class, 'show_charity_donation']);  // get current user projects
        
    });