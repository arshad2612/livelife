<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonationCategory extends Model
{
    use HasFactory;

    protected $table = 'donation_categories';

    protected $fillable = ['name', 'content','image'];

    public function CharityPrject(){
    	return $this->hasMany('CharityPrject');
    }
}
