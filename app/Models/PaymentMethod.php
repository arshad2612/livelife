<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory;

    protected $table = 'payment_methods';

   
    protected $fillable = ['user_id', 'card_name','card_number','cvc','expiry'];

    public function User(){
        return $this->belongsTo('User');
    }
}
