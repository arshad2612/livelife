<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CharityProject extends Model
{
    use HasFactory;
    protected $table = 'charity_projects';


    public function DonationCategory(){
        return $this->belongsTo('DonationCategory');
    }
}
