<?php

namespace App\Http\Controllers;
use App\Models\CharityProject;
use Illuminate\Http\Request;
use App\Http\Requests\CharityProjectRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CharityProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $charityProjects = CharityProject::all(); 
       
        return response()->json([
            'message' => 'Get All Charity Projects',
            'charityProjects' => $charityProjects
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'category_id'   => 'required',
            'user_id'       => 'required',
            'profession'    => 'required',
            'institution'   => 'required',
            'social_media'  => 'required',
            'address'       => 'required',
            'title'         => 'required',
            'benefit_recipients'   => 'required',
            'target'         => 'required',
            'deadline'       => 'required|date',
            'start_program'  => 'required|date',
            'end_program'    => 'required|date|after:start_program',
            'image'          => 'required',
            'description'    => 'required',
            'phone'    => 'required',
            ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

         $category_id   = $request->category_id;
         $user_id   = $request->user_id;
         $profession   = $request->profession;
         $institution   = $request->institution;
         $social_media   = $request->social_media;
         $address   = $request->address;
         $title   = $request->title;
         $benefit_recipients   = $request->benefit_recipients;
         $target   = $request->target;
         $deadline   = $request->deadline;
         $start_program   = $request->start_program;
         $end_program   = $request->end_program;
         $description   = $request->description;
         $phone   = $request->phone;

        //update link after live - remove livelife
        $url = url('/')."/storage/app/public/projectImages/";
        $upload = '/public/projectImages/';
        $image      = $request->file('image');
        $filename   = time()."-".$image->getClientOriginalName();
        
        $imageName = $url.$filename;
        $path = $request->image->storeAs($upload, $filename);

         DB::table('charity_projects')->insert([
            'category_id'   => $category_id,
            'user_id'       => $user_id,
            'profession'    => $profession,
            'institution'   => $institution,
            'social_media'  => $social_media,
            'address'       => $address,
            'title'         => $title,
            'benefit_recipients'   =>  $benefit_recipients,
            'target'         => $target,
            'deadline'       => $deadline,
            'start_program'  => $start_program,
            'end_program'    => $end_program,
            'image'          => $imageName,
            'description'    => $description,
            'phone'          => $phone,
            'active'         => 'no',
             
        ]);

        return response()->json([
            'message' => 'Successfully Create Donation Project'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = CharityProject::find($id);
        return response()->json([
            'message' => 'Get Single Charity Project',
            'project' => $project
        ], 200);
    }

    // show by category 
    public function showByCategory($id){
        $projectbyCat = CharityProject::where('category_id', $id)->get();
        return response()->json([
            'message' => 'fetch Charity Category wise Projects',
            'project' => $projectbyCat
        ], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
