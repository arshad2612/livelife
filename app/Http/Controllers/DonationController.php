<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\PaymentMethod;
use App\Models\Donation;
use App\Models\CharityProject;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class DonationController extends Controller
{
    

    public function create_donation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'project_id' => 'required',
            'amount' => 'required',
            'payment_method' => 'required',
            'status' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        Donation::create([
            'user_id' => $request->get('user_id'),
            'project_id' => $request->get('project_id'),
            'amount' => $request->get('amount'),
            'payment_method' => $request->get('payment_method'),
            'status' => $request->get('status')
        ]);

        return response()->json([
            'message' => 'Add Donation Successfully Created!'
        ], 201);
    }

    public function show_user_donation($id){

        $userDonation = DB::table('charity_projects')
            ->join('donations', 'charity_projects.id', '=', 'donations.project_id')
            ->select('*')
            ->where('donations.user_id', $id)
            ->get();

        return response()->json([
            'message' => 'Current User Donations',
            'myDonation' =>$userDonation
        ], 201);
    }

    public function show_user_projects($id){

        $myProject  = DB::table('charity_projects')->where('user_id', $id)->get();

        return response()->json([
            'message' => 'Current User Donation Projects',
            'myDonation' =>$myProject
        ], 201);
    }

    public function show_charity_donation($id){
        $charityDonation  = DB::table('donations')->where('project_id', $id)->get();

        return response()->json([
            'message' => 'Current User Donation Projects',
            'myDonation' =>$charityDonation
        ], 201);
    }


}
