<?php

namespace App\Http\Controllers;
use App\Models\DonationCategory;

use Illuminate\Http\Request;

class DonationCategoryController extends Controller
{
        
    // fetch all charity categories
    public function index() 
    {
        $donationCat = DonationCategory::all(); 
       
        return response()->json([
            'message' => 'Get All Charity Categories',
            'donationCat' => $donationCat
        ], 200);
    }
    
}