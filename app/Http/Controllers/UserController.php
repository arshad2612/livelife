<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\PaymentMethod;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
//use Illuminate\Support\Arr;



class UserController extends Controller
{
    
    // login information
    public function authenticate(Request $request)
    {
       
        $login_type = filter_var($request->get('email'), FILTER_VALIDATE_EMAIL ) ? 'email' : 'phone';
        $credentials = [$login_type => $request->get('email'), 'password'=>$request->get('password')];

        //$credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = auth()->user();

        return response()->json([
            'message' => 'User successfully Login',
            'token' => $token,
            'Login User' => $user
        ], 200);

    }

    // signup function
    public function register(Request $request)
    {
        if(is_numeric($request->get('email'))){

                $validator = Validator::make($request->all(), [
                    'email' => 'required|unique:users,phone',
                    'password' => 'required|string|min:6|confirmed',
                ],
                [
                   'email.unique' => 'The Phone Number has already been taken!'
                ]);
                
                if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
                }

                $data = ['phone'=>$request->get('email'),'password'=>Hash::make($request->get('password'))];
             
        }else{
                $validator = Validator::make($request->all(), [
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
                ]);
                $data = ['email' => $request->get('email'), 'password'=>Hash::make($request->get('password'))];

                if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
                 }
        }
        $user = User::create($data);
        $token = JWTAuth::fromUser($user);

        return response()->json([
            'message' => 'User successfully Registerd',
            'token' => $token,
            'user' =>$user
        ], 201);
    }

    //fetch single user information
    public function getAuthenticatedUser()
    {
               try {
                        if (! $user = JWTAuth::parseToken()->authenticate()) {
                                return response()->json(['user_not_found'], 404);
                        }
                } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                        return response()->json(['token_expired'], $e->getStatusCode());
                } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                        return response()->json(['token_invalid'], $e->getStatusCode());
                } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                        return response()->json(['token_absent'], $e->getStatusCode());
                }

                $paymentMethods = PaymentMethod::where('user_id', $user->id)->get();
            
                return response()->json([
                    'message' => 'Current User Profile',
                    'user' =>$user,
                    'paymentMethods' => $paymentMethods
                ], 201);
                
    }

    //update profile 
    public function update (Request $request, $id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
 
        //update link after live - remove livelife
        $url = url('/')."/storage/app/public/userImages/";
        $upload = '/public/userImages/';

        $data  = $request->file('photo'); 
        $extension = $request->photo->extension();
        $imageName1 = time().'.'.$extension ;
        $imageName = $url.$imageName1;
        $path = $request->photo->storeAs($upload, $imageName1);
        
        $user = User::find($id);
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email  = $request->email;
        $user->phone  = $request->phone;
        $user->photo  = $imageName;
        $user->save();

        return response()->json([
            'message' => 'User profile successfully updated!'
        ], 200);   
    }

    // payment method function
    public function show_payment_methods($id){

        $paymentMethods = PaymentMethod::where('user_id', $id)->get();
       
        return response()->json([
            'message' => 'Get All Current User Payment Methods',
            'paymentMethods' => $paymentMethods
        ], 200);
    }

    // create payment method function
    public function create_payment_methods(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'card_name' => 'required',
            'card_number' => 'required|max:16',
            'cvc' => 'required|max:3',
            'expiry' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        PaymentMethod::create([
            'user_id' => $request->get('user_id'),
            'card_name' => $request->get('card_name'),
            'card_number' => $request->get('card_number'),
            'cvc' => $request->get('cvc'),
            'expiry' => $request->get('expiry')
        ]);

        return response()->json([
            'message' => 'Payment Method Successfully Created!'
        ], 201);
    }

}
