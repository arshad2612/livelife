<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CharityProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => 'required',
            'profession'    => 'required',
            'institution'   => 'required',
            'social_media'  => 'required',
            'address'       => 'required',
            'title'         => 'required',
            'benefit_recipients'   => 'required',
            'target'         => 'required',
            'deadline'       => 'required|date',
            'start_program'  => 'required|date',
            'end_program'    => 'required|date|after:start_program',
            'image'          => 'required',
            'description'    => 'required',
            'phone'    => 'required',
        ];
    }
}
