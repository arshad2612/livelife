<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update2CharityProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('charity_projects', function (Blueprint $table) {
            $table->string('profession');
            $table->string('institution');
            $table->string('social_media');
            $table->text('address');
            $table->string('benefit_recipients');
            $table->dateTime('start_program');
            $table->dateTime('end_program');
            $table->string('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('charity_projects', function (Blueprint $table) {
            $table->dropColumn('profession','institution','social_media','address','benefit_recipients','start_program','end_program','phone');
        });
    }
}
